############################################
# Copyright (c) 2018 
#
# Written by 
############################################
import sys,os
import ConfigParser
from lib.log import *

class IniParser:
	def __init__(self, file):
		self.file = file
		self.cf = ConfigParser.ConfigParser()
		self.cf.read(self.file)

	def parserGet(self, section, item):
		return self.cf.get(section, item)

	def parserSet(self, section, item, value):
		self.cf.set(section, item, value)
		self.cf.write(open(self.file, "w"))

	def iniDump(self):
		total_sections = self.cf.sections()
		for section in total_sections:
			pp_debug( "[%s]" %(section))
			for item in self.cf.items(section):
				pp_debug("\t%s=%s" %(item[0], item[1]))

if __name__ == "__main__":
	p = IniParser("setup.ini")
	p.iniDump()
	p.parserGet("set", "file")
	p.parserGet("set", "pagesize")
	p.parserSet("get", "totalpages", 10)
