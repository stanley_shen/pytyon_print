1. 软件名称
=========
PrintProxy 打印机代理程序


2. 项目介绍
=========================================
学习和使用如何使用python读取和打印word和PDF文档
通过调用win32print来操作和设定打印机
通过调用word和PDF自身的程序来进行打印工作
（使用win32print和windows DC可以打印txt以及jpg文件，
   但是对于word PDF 等第三方文件格式的打印，没有较好的解决方案）


3. 软件架构
=======
略


4. 安装教程
=======
4.1 python
------------
本软件支持python2.7
请追加如下ptyhon包
 - pywin32
 - PyPDF2

4.2 office
----------
请安装微软的办公软件（2007或更高版本）
 - word 必须
 - 其他 可选

4.3 adobe reader
------------------
请安装adobe reader（10或更高版本）


5. 使用说明
=======
5.1 程序运行
------------
请将ptyhon.exe的目录设定到系统的PATH目录
然后再在任意目录运行python {path}/PrintProxy/main.py

5.2 ini文件配置
---------------
调用方和打印机代理程序(本程序)是通过{path}/PrintProxy/setup.ini文件来进行交互的。
使用者可以通过设定和读取setup.ini文件来控制打印过程，具体的设置内容如下：
app :     word或者adobe reader 的可执行文件目录，
             比如 C:\Program Files\Microsoft Office\Office15\WINWORD.EXE
                    C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe
file :       需要打印的文件目录，比如 C:\test.doc (注意目前支持的后缀是.doc .docx .pdf)
duplex : 是否双面打印 1 - 单面， 3 - 双面
color :    是否彩色打印 1 - 黑白，2 - 彩色
papersize : 纸张大小，请填写下面几种的一种 A3，A4
copies :  打印份数， 0以上的数字
level :     log打印的level，一般默认为info,可以按照输出级别设定为 debug，info，warning，error
result :    打印结果，正常返回为0，异常返回为-1
totalpages : 总的打印页数，请确保在result为0的时候再读取该值


6. 更新履历
=======
2018/08/18      v1.0 Intial version
